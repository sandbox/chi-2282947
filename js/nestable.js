(function ($) {
  Drupal.behaviors.nestable = {
    attach: function (context, settings) {

      Drupal.theme.prototype.nestableChangedWarning = function () {
        return '<div class="nestable-changed-warning messages warning">' + Drupal.t('Changes made in this table will not be saved until the form is submitted.') + '</div>';
      };

      var options = {
        listNodeName: 'ul',
        itemNodeName: 'li',
        rootClass: 'nestable',
        listClass: 'nestable-list',
        itemClass: 'nestable-item',
        dragClass: 'nestable-dragel',
        handleClass: 'nestable-handle',
        collapsedClass: 'nestable-collapsed',
        placeClass: 'nestable-placeholder',
        noDragClass: 'nestable-nodrag',
        emptyClass: 'nestable-empty',
       expandBtnHTML   : '<button data-action="expand" class="nestable-expand-list" type="button">+</button>',
        collapseBtnHTML : '<button data-action="collapse" class="nestable-collapse-list" type="button">-</button>',
        maxDepth: 99
      };

      var list = $('.nestable');

      var updateOutput = function (e) {
        var target = e.length ? e : $(e.target),
          output = target.data('output');
        if (window.JSON) {
          output.val(window.JSON.stringify(target.nestable('serialize')));
        }
      };

      list.nestable(options).on('change', updateOutput);
      list.nestable(options).on('change', function () {
        if (!this.changed) {
          $(Drupal.theme('nestableChangedWarning')).insertBefore($('.nestable-menu')).hide().fadeIn('slow');
          this.changed = true;
        }
      });
      updateOutput(list.data('output', $('.nestable-value')));

      var cookieName = 'Drupal.nestable-collapsed.' + list.attr('id');
      var cookieOptions = {
        path: Drupal.settings.basePath,
        // The cookie expires in one year.
        expires: 365
      };
      if ($.cookie(cookieName) == 1) {
        list.nestable('collapseAll');
      }

      $('#nestable-collapse-all').click(function (e) {
        list.nestable('collapseAll');
        $.cookie(cookieName, 1, cookieOptions);
        e.preventDefault();
      });

      $('#nestable-expand-all').click(function (e) {
        list.nestable('expandAll');
        $.cookie(cookieName, 0, cookieOptions);
        e.preventDefault();
      });

    }}
})(jQuery);

