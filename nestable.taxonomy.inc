<?php

/**
 * @file
 * Administrative page callbacks for the taxonomy module.
 */

/**
 * Form builder for the taxonomy terms overview.
 *
 * Display a tree of all the terms in a vocabulary, with options to edit
 * each one. The form is made drag and drop by the Nestable library.
 */
function nestable_overview_terms($form, &$form_state, $vocabulary) {

  if (!$tree = taxonomy_get_tree($vocabulary->vid)) {
    $form['no-terms'] = array(
      '#markup' => t('No terms available. <a href="@link">Add term</a>.', array('@link' => url('admin/structure/taxonomy/' . $vocabulary->machine_name . '/add'))),
    );
    return $form;
  }

  // Check for confirmation forms.
  if (isset($form_state['confirm_reset_alphabetical'])) {
    module_load_include('inc', 'taxonomy', 'taxonomy.admin');
    return taxonomy_vocabulary_confirm_reset_alphabetical($form, $form_state, $vocabulary->vid);
  }

  $hierarchy_index = array();
  foreach ($tree as $term) {
    $item = array(
      'title' => $term->name,
      'data' => $term->description,
      'data-id' => $term->tid,
    );

    $hierarchy_index[$term->parents[0]]['children'][$term->tid] = $item;
  }

  $items = nestable_build_term_items($hierarchy_index);

  $form['terms_list'] = array(
    '#type' => 'nestable_list',
    '#items' => $items,

  );

  $form['#vocabulary'] = $vocabulary;

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  $form['actions']['reset_alphabetical'] = array(
    '#type' => 'submit',
    '#value' => t('Reset to alphabetical'),
  );

  return $form;
}

/**
 * Build terms tree.
 */
function nestable_build_term_items($hierarchy_index, $tid = 0) {
  foreach ($hierarchy_index[$tid]['children'] as $item) {
    if (isset($hierarchy_index[$item['data-id']])) {
      $item['children'] = nestable_build_term_items($hierarchy_index, $item['data-id']);
    }
    $item['links'] = array(
      l(t('Edit'), 'taxonomy/term/' . $item['data-id'] . '/edit', array('query' => drupal_get_destination())),
    );
    $items[$item['data-id']] = $item;
  }
  return $items;
}


/**
 * Submit handler for terms overview form.
 */
function nestable_overview_terms_submit($form, &$form_state) {

  if ($form_state['triggering_element']['#value'] == t('Reset to alphabetical')) {

    // Execute the reset action.
    if ($form_state['values']['reset_alphabetical'] === TRUE) {

      module_load_include('inc', 'taxonomy', 'taxonomy.admin');
      return taxonomy_vocabulary_confirm_reset_alphabetical_submit($form, $form_state);
    }
    // Rebuild the form to confirm the reset action.
    $form_state['rebuild'] = TRUE;
    $form_state['confirm_reset_alphabetical'] = TRUE;
    return;
  }

  $row_values = drupal_json_decode($form_state['values']['terms_list']['json']);
  $values = nestable_process_values($row_values);
  $vocabulary = $form['#vocabulary'];
  $tree = taxonomy_get_tree($vocabulary->vid);
  $changed_weights = $changed_parents = array();

  foreach ($tree as $term) {
    if (isset($values[$term->tid])) {
      if ($values[$term->tid]['parent'] != $term->parents[0]) {
        $changed_parents[$term->tid] = $values[$term->tid]['parent'];
      }
      if ($values[$term->tid]['weight'] != $term->weight) {
        $changed_weights[$term->tid] = $values[$term->tid]['weight'];
      }
    }
  }

  foreach ($changed_parents as $tid => $changed_parent) {
    // Update term_hierachy and term_data directly since we don't have a
    // fully populated term object to save.
    db_update('taxonomy_term_hierarchy')
      ->fields(array('parent' => $changed_parent))
      ->condition('tid', $tid)
      ->execute();
  }

  foreach ($changed_weights as $tid => $changed_weight) {
    db_update('taxonomy_term_data')
      ->fields(array('weight' => $changed_weight))
      ->condition('tid', $tid)
      ->execute();
  }

  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Prepare nestable data to be submitted.
 */
function nestable_process_values($tree, $parent = 0) {
  $values = array();
  foreach ($tree as $weight => $item) {
    $values[$item['id']] = array(
      'weight' => $weight,
      'parent' => $parent,
    );
    if (isset($item['children'])) {
      $values = $values + nestable_process_values($item['children'], $item['id']);
    }
  }
  return $values;
}
