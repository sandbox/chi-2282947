<?php

/**
 * @file
 * Primary hooks for Nestable module.
 */

define('NESTABLE_LIBRARY_PATH', 'sites/all/libraries/nestable');

/**
 * Implements hook_menu_alter().
 */
function nestable_menu_alter(&$items) {

  $path = 'admin/structure/taxonomy/%taxonomy_vocabulary_machine_name';

  $items[$path]['page arguments'] = array('nestable_overview_terms', 3);
  $items[$path]['file'] = 'nestable.taxonomy.inc';
  $items[$path]['module'] = 'nestable';

  return $items;
}

/**
 * Implements hook_theme().
 */
function nestable_theme() {
  $items['nestable_list'] = array(
    'variables' => array(
      'items' => array(),
      'title' => NULL,
      'attributes' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_library().
 */
function nestable_library() {
  $libraries['nestable'] = array(
    'title' => 'Nestable',
    'website' => 'http://dbushell.github.io/Nestable/',
    'version' => 'development',
    'js' => array(
      NESTABLE_LIBRARY_PATH . '/jquery.nestable.js' => array(),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_element_info().
 */
function nestable_element_info() {
  $types['nestable_list'] = array(
    '#input' => TRUE,
    '#process' => array('nestable_process_element'),
    '#theme_wrappers' => array('form_element'),
  );
  return $types;
}

/**
 * Process a nestable form element.
 */
function nestable_process_element($element) {

  $element['nestable_menu'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('nestable-menu')),
  );

  $element['nestable_menu']['expand_link'] = array(
    '#prefix' => '<a href="#" id="nestable-expand-all">',
    '#markup' => t('Expand All'),
    '#suffix' => '</a>',
  );

  $element['nestable_menu']['collapse_link'] = array(
    '#prefix' => '<a href="#" id="nestable-collapse-all">',
    '#markup' => t('Collapse All'),
    '#suffix' => '</a>',
  );

  $element['list'] = array(
    '#theme' => 'nestable_list',
    '#items' => $element['#items'],
    '#input' => FALSE,
    '#attributes' => array(
      'id' => drupal_html_id($element['#id']),
      'class' => array('nestable'),
    ),
  );

  $element['json'] = array(
    '#type' => 'hidden',
    '#attributes' => array('class' => array('nestable-value')),
  );

  $element['#tree'] = TRUE;
  $element['#attached']['library'][] = array('nestable', 'nestable');
  $element['#attached']['library'][] = array('system', 'jquery.cookie');
  $element['#attached']['js'][] = drupal_get_path('module', 'nestable') . '/js/nestable.js';
  $element['#attached']['css'][] = drupal_get_path('module', 'nestable') . '/css/nestable.css';

  return $element;
}

/**
 * Returns HTML for a nestable list of items.
 */
function theme_nestable_list($vars) {

  // Do not wrap nested lists.
  $output = empty($vars['nested']) ? '<div' . drupal_attributes($vars['attributes']) . '>' : '';

  if (!empty($vars['items'])) {
    $output .= '<ul class="nestable-list">';
    foreach ($vars['items'] as $item) {
      $attributes = array();
      $attributes['class'][] = 'nestable-item';
      $children = array();
      $data = $links = '';

      foreach ($item as $key => $value) {
        if ($key == 'title') {
          $data .= '<span class="nestable-item-title">' . $value . '</span>';
        }
        elseif ($key == 'data') {
          $data .= $value;
        }
        elseif ($key == 'children') {
          $children = $value;
        }
        elseif ($key == 'links') {
          $data .= '<div class="nestable-item-links">' . implode(' ', $value) . '</div>';
        }
        else {
          $attributes[$key] = $value;
        }
      }

      $data = '<div class="nestable-handle"></div><div class="nestable-item-content">' . $data . '</div>';

      if (count($children) > 0) {
        // Render nested list.
        $data .= theme(
          'nestable_list',
          array(
            'nested' => TRUE,
            'items' => $children,
            'attributes' => $attributes,
          )
        );
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= '</ul>';
  }
  $output .= empty($vars['nested']) ? '</div>' : '';

  return $output;
}
