<?php

/**
 * @file
 * Drush integration for Nestable module.
 */

/**
 * The Nestaole library URL.
 */
define('NESTABLE_DOWNLOAD_URL', 'https://raw.githubusercontent.com/dbushell/Nestable/master/jquery.nestable.js');

/**
 * Implements hook_drush_command().
 */
function nestable_drush_command() {
  $items['nestable-download'] = array(
    'description' => dt('Download and install Nestable library.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );
  return $items;
}

/**
 * Command to download the Nestable library.
 */
function drush_nestable_download() {

  $libraries_dir = 'sites/all/libraries';
  $target_dir = $libraries_dir . '/nestable';

  if (!is_dir($libraries_dir)) {
    return drush_set_error('NESTABLE', dt('Directory !libraries_dir does not exitst.', array('!libraries_dir' => $libraries_dir)));
  }

  if (is_dir($target_dir)) {
    if (drush_confirm(dt('Install location !target_dir already exists. Do you want to overwrite it?', array('!target_dir' => $target_dir)))) {
      drush_delete_dir($target_dir, TRUE);
    }
    else {
      return drush_log(dt('Aborting.'));
    }
  }

  drush_mkdir($target_dir);

  drush_shell_exec('wget --timeout=5 -O %s %s', $target_dir . '/jquery.nestable.js', NESTABLE_DOWNLOAD_URL);
  if (!drush_file_not_empty($target_dir)) {
    return drush_set_error('NESTABLE', dt('Could not download Nestable library from GitHub.'));
  }

  if (is_dir($target_dir)) {
    drush_log(dt('Nestable library has been installed in @path', array('@path' => $target_dir)), 'success');
  }
  else {
    return drush_set_error('NESTABLE', dt('Drush was unable to install the Nestable library.'));
  }

}
