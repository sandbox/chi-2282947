-= Nestable Drupal module =-

Summary
=========================
This module is an integration with Nestable jQuery plugin.
It provides a new form element "nestable_list" which can be used
as replacement for core tabledrag widgets.

You can test how it works on taxonomy terms overview page.
See admin/structure/taxonomy/VOCABULARY_NAME

Requirements
=========================
 * Nestable jQuery Plugin
 * jQuery 1.7 or higher (use Jquery Update or jQuery Multi module)
 
Installation
=========================
 * Install as usual, see http://drupal.org/node/895232 for further information.
 * Download Nestable jQuery plugin from https://github.com/dbushell/Nestable.
 * Place nestable.jquery.js file to sites/all/libraries/nestable directory.

Notes
=========================
 * You can use `drush nestable-download` command for easy installation of the library.
